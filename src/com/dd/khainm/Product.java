package com.dd.khainm;

import java.util.*;

public class Product {

	// Trường
	private int id, categoryId, quality;
	private String name;
	private Date saleDate;
	private boolean isDeleted;

	// Hàm khởi tạo
	Product(int id, int categoryId, int quality, String name, Date saleDate, boolean isDeleted) {

		this.id = id;
		this.categoryId = categoryId;
		this.quality = quality;

		this.name = name;

		this.saleDate = saleDate;

		this.isDeleted = isDeleted;
	}

	// Hàm set
	public void setId(int id) {
		this.id = id;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public void setQuality(int quality) {
		this.quality = quality;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSaleDate(Date saleDate) {
		this.saleDate = saleDate;
	}

	public void setIsDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	// Hàm get
	public int getId() {
		return this.id;
	}

	public int getCategoryId() {
		return this.categoryId;
	}

	public int getQuality() {
		return this.quality;
	}

	public String getName() {
		return this.name;
	}

	public Date getSaleDate() {
		return this.saleDate;
	}

	public boolean getIsDeleted() {
		return this.isDeleted;
	}

	// Hàm khác
	@Override
	public String toString() {
		return "+ ID: " + this.getId() + "\n+ Name: " + this.getName() + "\n+ CategoryID: " + this.getCategoryId()
				+ "\n+ Sale Date: " + this.getSaleDate() + "\n+ Quality: " + this.getQuality() + "\n+ Is deleted: "
				+ this.getIsDeleted() + "\n";
	}

	public static ArrayList<Product> listProduct(int[] ids, int[] categoryIds, int[] qualities, String[] names,
			Date[] saleDates, boolean[] isDeleteds) {

		// length các tham số truyền vào phải bằng nhau
		int num = ids.length;

		ArrayList<Product> result = new ArrayList<Product>();

		for (int i = 0; i < num; i++) {
			result.add(new Product(ids[i], categoryIds[i], qualities[i], names[i], saleDates[i], isDeleteds[i]));
		}

		return result;

	}

	public static String filterProductById(ArrayList<Product> listProduct, int idProduct, boolean withStream) {

		if (withStream) {
			Product productName = listProduct.stream().filter((product) -> product.getId() == idProduct)
					.findAny().orElse(null);

			try {
				return productName.getName();
			}
			catch(NullPointerException e){
				return "No product";
			}
		}

		// FIXME
		String[] productName = new String[1];

		listProduct.forEach((product) -> {
			if (product.getId() == idProduct) {
				productName[0] = product.getName();
			}
		});

		return productName.length == 1 && productName[0] != null ?  productName[0] : "No product";

	}

	public static Product[] filterProductByQuality(ArrayList<Product> listProduct, boolean withStream) {

		if (withStream) {
			Product[] products = listProduct.stream().filter((product) -> product.getQuality() > 0)
					.toArray(Product[]::new);

			return products;
		}

		ArrayList<Product> productsStore = new ArrayList<Product>();

		listProduct.forEach((product) -> {
			if (product.getQuality() > 0) {
				productsStore.add(product);
			}
		});

		return productsStore.toArray(new Product[productsStore.size()]);

	}

	public static Date trim(Date date) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.HOUR, 0);

		return calendar.getTime();
	}

	public static Product[] filterProductBySaleDateWithoutDeleted(ArrayList<Product> listProduct, boolean withStream) {

		Date today = new Date();
		Date todayFormatted = Product.trim(today);

		if (withStream) {
			Product[] products = listProduct.stream()
					.filter((product) -> (Product.trim(product.getSaleDate()).compareTo(todayFormatted) > 0)
							&& !product.getIsDeleted())
					.toArray(Product[]::new);

			return products;
		}

		ArrayList<Product> productsStore = new ArrayList<Product>();

		listProduct.forEach((product) -> {
			if ((Product.trim(product.getSaleDate()).compareTo(todayFormatted) > 0) && !product.getIsDeleted()) {
				productsStore.add(product);
			}
		});

		return productsStore.toArray(new Product[productsStore.size()]);
	}

	public static int totalProduct(ArrayList<Product> listProduct, boolean withReducer) {

		if (withReducer) {
			Optional<Integer> total = listProduct.stream().filter((product) -> !product.getIsDeleted())
					.map((product) -> product.getQuality()).reduce((quality1, quality2) -> quality1 + quality2);

			return total.get();
		}

		int total = listProduct.stream().filter((product) -> !product.getIsDeleted())
				.map((product) -> product.getQuality()).mapToInt(Integer::intValue).sum();

		return total;

	}

	public static boolean isHaveProductInCategory(ArrayList<Product> listProduct, int categoryId, boolean withStream) {

		if (withStream) {

			boolean isHave = listProduct.stream().anyMatch((product) -> product.getCategoryId() == categoryId);

			return isHave;
		}

		for (int index = 0; index < listProduct.size(); index++) {
			if (listProduct.get(index).getCategoryId() == categoryId) {
				return true;
			}
		}

		return false;
	}

	public static String[][] filterProductBySaleDateWithQuality(ArrayList<Product> listProduct, boolean withStream) {

		Date today = new Date();
		Date todayFormatted = Product.trim(today);

		if (withStream) {

			String[][] products = listProduct.stream()
					.filter((product) -> Product.trim(product.getSaleDate()).compareTo(todayFormatted) > 0
							&& product.getQuality() > 0)
					.map((product) -> new String[] { Integer.toString(product.getId()), product.getName() })
					.toArray(String[][]::new);

			return products;
		}

		ArrayList<String[]> productsStore = new ArrayList<String[]>();

		listProduct.forEach((product) -> {
			if (Product.trim(product.getSaleDate()).compareTo(todayFormatted) > 0 && product.getQuality() > 0) {
				productsStore.add(new String[] { Integer.toString(product.getId()), product.getName() });
			}
		});

		return productsStore.toArray(new String[productsStore.size()][2]);
	}

	public static void main(String[] args) {
		// TEST
		int[] ids = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		int[] categoryIds = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		int[] qualities = new int[] { -5, -4, -3, -2, -1, 1, 2, 3, 4, 5 };

		String[] names = new String[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "k" };
		Date[] saleDates = new Date[] { new Date(), new Date(), new Date(), new Date(), new Date(), new Date(),
				new Date(), new Date(), new Date(), new Date() };
		boolean[] isDeleteds = new boolean[] { true, false, true, false, true, false, true, false, true, false };

		ArrayList<Product> list_product = Product.listProduct(ids, categoryIds, qualities, names, saleDates,
				isDeleteds);
		
		System.out.println("All products:");
		
		list_product.forEach((product) -> {
			System.out.println(product.toString());
		});
		
		
		// TEST: filterProductById
		System.out.println("------------------------------------------------------------");
		System.out.println("TEST: filterProductById");
		System.out.println("\nFind product with ID 1 without stream: " + Product.filterProductById(list_product, 0, false)); // without stream
		System.out.println("Find product with ID 1 with stream: " + Product.filterProductById(list_product, 0, true)); // with stream
		
		// TEST: fiterProductByQuality
		System.out.println("------------------------------------------------------------");
		System.out.println("TEST: fiterProductByQuality");
		
		Product[] productQualityA = Product.filterProductByQuality(list_product, false); // without stream
		Product[] productQualityB = Product.filterProductByQuality(list_product, true); // with stream
		
		System.out.println("\nWithout stream:\n");
		for(Product product : productQualityA) {
			System.out.println(product.toString());
		}
		
		System.out.println("With stream:\n");
		for(Product product : productQualityB) {
			System.out.println(product.toString());
		}
		
		// TEST: filterProductBySaleDateWithoutDeleted
		System.out.println("------------------------------------------------------------");
		System.out.println("TEST: filterProductBySaleDateWithoutDeleted");
		
		Product[] products1 = Product.filterProductBySaleDateWithoutDeleted(list_product, false); // without stream
		Product[] products2 = Product.filterProductBySaleDateWithoutDeleted(list_product, true); // with stream
		
		System.out.println("\nWithout stream:\n");
		for(Product product : products1) {
			System.out.println(product.toString());
		} // Trống do cùng ngày
		
		System.out.println("With stream:\n");
		for(Product product : products2) {
			System.out.println(product.toString());
		} // Trống do cùng ngày
		
		// TEST: totalProduct
		System.out.println("------------------------------------------------------------");
		System.out.println("TEST: totalProduct");
		
		int total1 = Product.totalProduct(list_product, false); // without Reducer
		int total2 = Product.totalProduct(list_product, false); // with Reducer
		
		System.out.println("\nTotal without Reducer: " + total1);
		System.out.println("Total with Reducer: " + total2);
		
		// TEST: isHaveProductInCategory
		System.out.println("------------------------------------------------------------");
		System.out.println("TEST: isHaveProductInCategory");
		
		boolean isHave1 = Product.isHaveProductInCategory(list_product, 5, false); // without stream
		boolean isHave2 = Product.isHaveProductInCategory(list_product, 5, true); // with stream
		
		System.out.println("\nProduct with categoryID 5 (without stream): " + isHave1);
		System.out.println("Product with categoryID 5 (with stream): " + isHave2);
		
		// TEST: filterProductBySaleDateWithQuality
		System.out.println("------------------------------------------------------------");
		System.out.println("TEST: filterProductBySaleDateWithQuality");
		
		String[][] product3 = Product.filterProductBySaleDateWithQuality(list_product, false); // without stream
		String[][] product4 = Product.filterProductBySaleDateWithQuality(list_product, true); // with stream
		
		System.out.println("\nWithout stream:\n");
		for(String[] product : product3) {
			System.out.println("ID: " + product[0] + "\nName: " + product[1] + "\n");
		} // Trống do cùng ngày
		
		System.out.println("\nWith stream:\n");
		for(String[] product : product4) {
			System.out.println("ID: " + product[0] + "\nName: " + product[1] + "\n");
		} // Trống do cùng ngày
	}
}
